An application to run a simulation of the relative agreement model:

Deffuant et al. (2002): "How can extremism prevail? A study based on the relative agreement interaction model", 
JASS, 5 (4).

Application takes an object-oriented approach. It was used as an exercise in a seminar about 
agent-based modelling at Information and Decision Support Center in Cairo, (Egypt).

User must give as input:
-Number of agents, as a positive integer number.
-Number of iterations, as a positive integer number.
-Seed, as a positive integer number.
-Proportion of extremists, as a double in (0,1).
-Uncertainty, a double number greater than 0.